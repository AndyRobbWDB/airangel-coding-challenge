

#Airangel coding challenge

The challenge is to demonstrate my suitability for the Senior/Lead Developer at Airangel. To this end I have authored a game of Draghts that following the specification found here https://github.com/davidwhitney/CodeDojos/tree/master/Draughts.

The solution is written in TypeScript and utilises the Angular JavaScript framework v11.

The board is created in HTML from an array of arrays 8 x 8, these squares are instances of a JavaScript class that holds its position via the constructor, it also has 2 functions addPiece and clearPiece that facilitates the adding or removal of a piece to a square.
  
The board is created on initialisation of the app or by pressing the reset button.

The array of squares is used to generate the html for the board along with the pieces via the game-board Angular component, this component holds pretty much all the logic required by the game.

Black goes first, a turn begins by the player clicking on a square,an instance of an Angualar component, that may or may not contain a piece, if the square holds a piece then the click event bubbles up to an event in the game-board component that governs whether or not a piece can be moved. 

This is achieved by looking at at the squares diagonally around to determine whether or not they are occupied by a piece, if they are and the piece happens to be an opponents piece then the next diagonal is checks to see if its free, if so then this opponent's piece can be taken. 

All potential moves are indicated on the board via object properties which are used to modify css classes to bring in different colours. The player can then pick which square they would like to move their piece to.

If an opponents piece is taken then the board checks to see if there is a follow up move where another piece can be taken and so on.

Pieces can only move forward unless they have reached the far end of the board when they become a King which can then move forward and backwards, this status is indicated by setting a property of a piece that triggers the addition of a King character to the peiece.

Players take it in turn to move their pieces until one player can now longer move, either by the fact all their pieces have been taken or that none of their pieces can move.

    


Even though this game is pretty much for purpose, a production version would benefit from the following:
- Subresource Integrity
- Cache busting for new deployments
- Visible versioning
- Game state held in local storage or server
- Unit tests
- Continuous Integration / Continuous Delivery mechanism

Trade-offs you might have made, anything you left out, or what you might do differently if you were to spend additional time on the project.
I d would have preferred to supply an Angular version that I had coded.

Link to other code you're particularly proud of.
https://luton.refernet.co.uk/
https://fenix-insight.online/

Link to your resume or public profile.
https://www.linkedin.com/in/andyrobb1/

Link to the hosted application where applicable.
https://airangel-coding-challenge.web.app/

