import { Component, OnInit } from '@angular/core';
import { GameService }	   from '../game.service';
import { Piece }	from '../piece';
import { Square }	from '../square';
import { BehaviorSubject , Observable}      from 'rxjs';

@Component({
  selector: '[app-game-board]',
  templateUrl: './game-board.component.html',
  styleUrls: ['./game-board.component.scss']
})
export class GameBoardComponent implements OnInit {
  public board: any;
  private _currentPiece!: Piece;
  private _redTurn: boolean = false;
  private _doubleJump: boolean = false;

  public resetGame$?: Observable<boolean>;

  constructor(
     private service: GameService
  ) { }

  ngOnInit(): void {
    this.resetGame$ = this.service.resetGameObs;
    this.resetGame$.subscribe(reset => {
        if (reset) {
            this.onReset();
        }
    });
    this.onReset()
  }

  onReset() {
    this._redTurn = false;
    this.board = this.service.board;
  }
    // Determining if someone won the game
    isWinner(turn: boolean) {
        let redTeam: Piece[] = [];
        let blackTeam: Piece[] = [];
        let winner: boolean = true;

        // Collecting the pieces on the board into arrays


        for (var i = 0 ; i < this.board.length ; i++) {
          for (var j = 0 ; j < this.board[i].length ; j++) {

            let square: Square =  this.board[i][j];
            if (square.piece !== null && square.piece !== undefined) {
              if (square.piece.isRed) {
                  redTeam.push(square.piece);
              } else {
                  blackTeam.push(square.piece);
              }
            }
          }
        }



        // Check each team to see if someone won
        if (turn) { // red turn
            redTeam.forEach(piece => {
                if (this.canMove(piece)) { // if any piece can move then no winner
                    winner = false;
                }
            });
        } else { // black turn
            blackTeam.forEach(piece => {
                if (this.canMove(piece)) { // if any piece can move then no winner
                    winner = false;
                }
            });
        }

        if (winner) {
            let win = "none";
            if (turn) {
                win = "Black";
            } else {
                win = "Red";
            }
            if (win !== 'none') {
              this._redTurn = !this._redTurn;
              this.service.redTurn = this._redTurn;
              this.service.gameOver = true;
              switch (win) {
                case "Red":
                  this.service.redWincount++
                  break;
                case "Black":
                  this.service.blackWincount++
                  break;
                default:
                  break;
              }
            }
        }
    }

    // Click events for pieces and spaces

    // Click on a piece on the board
    onClickAPiece(p: Piece) {
        if (this._doubleJump) {
            if (this._currentPiece === p) {
                this.clearSelections();
                this._currentPiece = p;
                this.findPiece(p).highlight = true;
                this.selectMoveableSquares(p);
            }
        } else if (this._redTurn === p.isRed) {
            this.clearSelections();
            this._currentPiece = p;
            this.findPiece(p).highlight = true;
            this.selectMoveableSquares(p);
        }
    }

    // Click on an empty space on the board
    onClickEmptySquare(sp: Square) {
        if (this._currentPiece !== null && sp.moveTo) { // If the space is empty and piece can move to it
            this.findPiece(this._currentPiece).clearPiece(); // First remove piece from old space
            sp.addPiece(this._currentPiece); // Then add piece to the new space
            if (sp.jump === true) { // A piece was jumped
                this.clearJumpedPiece(sp);
            }
            if (this.isEndSquare(sp)) { // Selected piece became king
                this.makeKing(this._currentPiece);
            }
            if (sp.jump === false || !this.checkForJump(sp)) { // if I didn't just jump or there is no jump
                this._redTurn = !this._redTurn;
                this.service.redTurn = this._redTurn;
                this._doubleJump = false;
                this.clearSelections();
                this.isWinner(this._redTurn);
            } else { // double jump opportunity
                this._doubleJump = true;
                this.onClickAPiece(this._currentPiece);
            }
        }
    }

    // Finding the 4 potential move spaces for a piece.  If a piece can't move in a direction, returns null
    findMoveableSquares(p: Piece, preventStateChange: boolean) {
        // Calculating the 4 potential move spaces of the piece
        let upRight = null;
        let downRight = null;
        let upLeft = null;
        let downLeft = null;

        // If it's not the piece's second turn, get all move spaces, else only get the jump spaces
        if (!this._doubleJump) {

          upRight = this.getDiagMoveSquare(p, this.calcAllDiag(p).upRightDiag.sp, this.calcAllDiag(p).upRightDiag.diag, preventStateChange);
          downRight = this.getDiagMoveSquare(p, this.calcAllDiag(p).downRightDiag.sp, this.calcAllDiag(p).downRightDiag.diag, preventStateChange);
          upLeft = this.getDiagMoveSquare(p, this.calcAllDiag(p).upLeftDiag.sp, this.calcAllDiag(p).upLeftDiag.diag, preventStateChange);
          downLeft = this.getDiagMoveSquare(p, this.calcAllDiag(p).downLeftDiag.sp, this.calcAllDiag(p).downLeftDiag.diag, preventStateChange);
        } else {
            upRight = this.getDiagJumpSquare(p, this.calcAllDiag(p).upRightDiag.sp, this.calcAllDiag(p).upRightDiag.diag, preventStateChange);
            downRight = this.getDiagJumpSquare(p, this.calcAllDiag(p).downRightDiag.sp, this.calcAllDiag(p).downRightDiag.diag, preventStateChange);
            upLeft = this.getDiagJumpSquare(p, this.calcAllDiag(p).upLeftDiag.sp, this.calcAllDiag(p).upLeftDiag.diag, preventStateChange);
            downLeft = this.getDiagJumpSquare(p, this.calcAllDiag(p).downLeftDiag.sp, this.calcAllDiag(p).downLeftDiag.diag, preventStateChange);
        }

        return {
            upRight: upRight,
            downRight: downRight,
            upLeft: upLeft,
            downLeft: downLeft
        }
    }

    // Highlight and set moveTo flag on spaces a piece could move to
    selectMoveableSquares(p: Piece) {
        // Calculating the 4 potential move spaces of the piece
        let { upLeft, upRight, downLeft, downRight } = this.findMoveableSquares(p, false);

        // If any of the potential move spaces exist, highlight and set moveTo flag
        if (upLeft !== null) {
            upLeft.highlight = upLeft.moveTo = true;
        }

        if (upRight !== null) {
            upRight.highlight = upRight.moveTo = true;
        }

        if (downLeft !== null) {
            downLeft.highlight = downLeft.moveTo = true;
        }

        if (downRight !== null) {
            downRight.highlight = downRight.moveTo = true;
        }

    }

    // For win checking.  If the piece can move, return true, otherwise false
    canMove(p: Piece): boolean {
        // Calculating the 4 potential move spaces of the piece

        let preventStateChange = true;

        let {upRight, downRight, upLeft, downLeft } = this.findMoveableSquares(p,preventStateChange)

        if (upRight == null && downRight == null && upLeft == null && downLeft == null) {
            return false;  // Can't move in any direction
        } else {
            return true;  // Able to move in at least one direction
        }
    }

    // Jumping

    // Given a space that a piece has moved to, find the piece that was jumped and clear it out
    clearJumpedPiece(sp: Square) {
        let pieces = new Array();

        pieces.push(this.getPiece(sp.row - 1, sp.col - 1));
        pieces.push(this.getPiece(sp.row - 1, sp.col + 1));
        pieces.push(this.getPiece(sp.row + 1, sp.col - 1));
        pieces.push(this.getPiece(sp.row + 1, sp.col + 1));

        this.findPiece(pieces.find(p => p !== null && p !== undefined && p.jump === true)).clearPiece();

    }

    // Check and see if there is a potential jump opportunity, for multi jump
    checkForJump(sp: Square): boolean {
        let p = sp.piece;

        if (this.canJump(p, this.calcAllDiag(p).upRightDiag.sp, this.calcAllDiag(p).upRightDiag.diag) ||
            this.canJump(p, this.calcAllDiag(p).downRightDiag.sp, this.calcAllDiag(p).downRightDiag.diag) ||
            this.canJump(p, this.calcAllDiag(p).upLeftDiag.sp, this.calcAllDiag(p).upLeftDiag.diag) ||
            this.canJump(p, this.calcAllDiag(p).downLeftDiag.sp, this.calcAllDiag(p).downLeftDiag.diag) ) {
            return true;
        } else {
            return false;
        }

    }

    // Can Jump - returns true if you can jump, if not then false
    canJump(p: Piece, sp: Square, diag: Square): boolean {
        if (sp === null || diag === null || sp.piece === undefined || sp.piece === null) {
            return false;
        } else if (p.isRed === !sp.piece.isRed && diag !== null && (diag.piece === null || diag.piece === undefined)) {
            return true;
        } else {
            return false;
        }
    }

    // Diagonal stuff

    // Returns all 4 diagonals
    calcAllDiag(p: Piece) {
        return {
            upRightDiag: this.calcDiag(p, true, true),
            downRightDiag: this.calcDiag(p, false, true),
            upLeftDiag: this.calcDiag(p, true, false),
            downLeftDiag: this.calcDiag(p, false, false)
        }
    }

    // Given a piece, and the direction, calculate the "diagonal" - the spaces on the right or left diagonally
    // Returns the piece on the space, the neighbor space and the space 2 up
    calcDiag(p: Piece, up: boolean, right: boolean) {
        let neighborRow = -1; // -1 means not on board
        let neighborCol = -1;
        let diagRow = -1;
        let diagCol = -1;

        if (up  && p.isKing) {
            if (right) {
                neighborRow = (<Piece>p).getUpRightMove().row;
                neighborCol = (<Piece>p).getUpRightMove().col;
                diagRow = (<Piece>p).getDiagUpRightMove().row;
                diagCol = (<Piece>p).getDiagUpRightMove().col;
            }
            if (!right) {
                neighborRow = (<Piece>p).getUpLeftMove().row;
                neighborCol = (<Piece>p).getUpLeftMove().col;
                diagRow = (<Piece>p).getDiagUpLeftMove().row;
                diagCol = (<Piece>p).getDiagUpLeftMove().col;
            }
        } else if (!up) {
            if (right) {

                neighborRow = (<Piece>p).getDownRightMove().row;
                neighborCol = (<Piece>p).getDownRightMove().col;
                diagRow = (<Piece>p).getDiagDownRightMove().row;
                diagCol = (<Piece>p).getDiagDownRightMove().col;
            }
            if (!right) {
                neighborRow = (<Piece>p).getDownLeftMove().row;
                neighborCol = (<Piece>p).getDownLeftMove().col;
                diagRow = (<Piece>p).getDiagDownLeftMove().row;
                diagCol = (<Piece>p).getDiagDownLeftMove().col;
            }
        }

        return {
            p: p,
            sp: this.checkBoardSquare(neighborRow, neighborCol),
            diag: this.checkBoardSquare(diagRow, diagCol)
        }
    }

    // Given a diagonal (a space and the next space up) return the space you can move to or null if you can't move
    getDiagMoveSquare(p: Piece, sp: Square, diag: Square, preventStateChange: boolean): Square {
        let space: Square = null!;

        if (sp !== null) {
            if (sp.piece === undefined || sp.piece === null) { // nextdoor is empty
                space = sp;
            } else if (this.canJump(p, sp, diag)) { // piece to jump
                if (!preventStateChange) {
                  sp.piece.jump = diag.jump = true; // set jump flag on piece to jump and diag space
                }
                space = diag;
            } else { // can't move down this diag
                space = null!;
            }
        }

        return space;
    }

    // Give a diagonal return space you can jump to or null if you can't jump
    getDiagJumpSquare(p: Piece, sp: Square, diag: Square, preventStateChange: boolean ): Square {
        let space: Square = null!;

        if (sp !== null) {
            if (this.canJump(p, sp, diag)) { // piece to jump
                if (!preventStateChange) {
                  sp.piece.jump = diag.jump = true; // set jump flag on piece to jump and diag space
                }
                space = diag;
            } else { // can't move down this diag
                space = null!;
            }
        }

        return space;
    }

    // King stuff

    // When a pawn makes it to the end of the board, replace the pawn piece with a king piece
    makeKing(p: Piece) {
        p.isKing = true;
    }

    // Given a space, return true if it is in an end row and false if it is not
    isEndSquare(sp: Square): boolean {
        if (sp.row === 0 || sp.row === 7) {
            return true;
        } else {
            return false;
        }
    }

    // Square utilities

    // Given a row and column that may or may not be on the board, check if it is on the board.  If it is return the space.
    checkBoardSquare(row: number, col: number): Square {
        if (row < 8 && row > -1 && col < 8 && col > -1) {
            return this.board[row][col];
        } else {
            return null!;
        }
    }

    // Find pieces

    findPiece(p: Piece): Square {
        let sq: Square = new Square(false,-1,-1);

        for (var i = 0 ; i < this.board.length ; i++) {
          for (var j = 0 ; j < this.board[i].length ; j++) {
            if (this.board[i][j].piece === p) {
                sq = this.board[i][j];
            }
          }
        }
        return sq;
    }

    // Given a row and column, return a piece if there is one
    getPiece(row: number, col: number): Piece {
        let square = this.checkBoardSquare(row, col);

        if (square !== null && square.piece !== null) {
            return square.piece;
        } else {
            return new Piece("none",0,0,false);
        }
    }

    // Clears all highlights, direction flags, and selected pieces from board
    clearSelections() {
      //debugger;
      for (var i = 0 ; i < this.board.length ; i++) {
        for (var j = 0 ; j < this.board[i].length ; j++) {
          this.board[i][j].highlight = this.board[i][j].moveTo = this.board[i][j].jump = false;
          if (this.board[i][j].piece !== null) {
            //this.board[i][j].piece.jump = false;
          }
        }
      }

        /*this.board.forEach(row => row.forEach(square => {
            square.highlight = square.moveTo = square.jump = false;
            if (square.piece !== null) {
                square.piece.jump = false;
            }
        }));*/
    }


}
