import { Injectable } from '@angular/core';
import { Board }	        from './board';
import { Piece }	from './piece';
import { BehaviorSubject }      from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  public board: any;
  redWincount: number = 0;
  blackWincount: number = 0;
  redTurn: boolean = false;
  gameOver: boolean = false;

  private _resetGame: BehaviorSubject<boolean>;

  constructor() {
    this._resetGame = <BehaviorSubject<boolean>>new BehaviorSubject(true);
    this._resetGame.subscribe(reset => {
        if (reset) {
          this.resetGame();
        }
    });
  }

  resetGame() {

    this.board = new Board().squares;
    this.redTurn = false;
    this.gameOver = false;

    let isKing = false;

    
    for (let i = 0; i < 3; i++) {
          for (let j = 0; j < 8; j++) {
            if (this.board[i][j].playable === true) {
              this.board[i][j].addPiece(new Piece('black', i, j, isKing));
            }
        }
    }
    for (let i = 5; i < 8; i++) {
            for (let j = 0; j < 8; j++) {
            if (this.board[i][j].playable === true) {
                this.board[i][j].addPiece(new Piece('red', i, j, isKing));
            }
        }
    }

  }

  // Getting the observables or behavior subjects for other components

  // For Game Status
  get resetGameBeh() {
      return this._resetGame;
  }

  // For Game Board
  get resetGameObs() {
      return this._resetGame.asObservable();
  }

}
