import { Component, OnInit } from '@angular/core';
import { GameService }	   from '../game.service';
import { BehaviorSubject }      from 'rxjs';

@Component({
  selector: '[app-game-status]',
  templateUrl: './game-status.component.html',
  styleUrls: ['./game-status.component.scss']
})
export class GameStatusComponent implements OnInit {

  public resetGame$?: BehaviorSubject<boolean>;;

  constructor(
     public service: GameService
  ) { }

  ngOnInit(): void {
    this.resetGame$ = this.service.resetGameBeh;
  }
  
  resetGame(reset: boolean) {
      this.resetGame$?.next(reset);
  }
}
