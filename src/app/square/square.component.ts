import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { GameService }	   from '../game.service';
import { Square }			from '../square';
import { Piece }	from '../piece';

@Component({
  selector: 'app-square',
  templateUrl: './square.component.html',
  styleUrls: ['./square.component.scss']
})
export class SquareComponent implements OnInit {
  @Input() square!: Square;
  @Output() clickAPiece = new EventEmitter<Piece>();
  @Output() clickEmptySpace = new EventEmitter<Square>();

  constructor(
       private service: GameService
  ) { }

  ngOnInit(): void {

  }

  
}
